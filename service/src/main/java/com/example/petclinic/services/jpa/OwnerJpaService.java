package com.example.petclinic.services.jpa;

import com.example.petclinic.entity.Owner;
import com.example.petclinic.services.OwnerService;
import com.example.petclinic.services.config.JpaImplementation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

@JpaImplementation
public class OwnerJpaService extends AbstractJpaService<Owner, Long> implements OwnerService {

    @Override
    public Collection<Owner> findByName(String name) {
        throw new UnsupportedOperationException();
    }

    @Override
    public JpaRepository<Owner, Long> getRepository() {
        throw new UnsupportedOperationException();
    }
}
