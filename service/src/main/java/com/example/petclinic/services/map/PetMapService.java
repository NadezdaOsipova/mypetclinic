package com.example.petclinic.services.map;

import com.example.petclinic.entity.Owner;
import com.example.petclinic.entity.Pet;
import com.example.petclinic.services.PetService;
import com.example.petclinic.services.config.MapImplementation;

import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class PetMapService extends AbstractMapService<Pet, Long> implements PetService {

    private static final Map<Long, Pet> resource = new HashMap<>();

    @Override
    public Map<Long, Pet> getResource() {
        return resource;
    }

    @Override
    public Pet findByOwner(Owner owner) {
        return null;//todo
    }
}
