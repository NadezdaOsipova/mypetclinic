package com.example.petclinic.services.map;

import com.example.petclinic.entity.Vet;
import com.example.petclinic.services.VetService;
import com.example.petclinic.services.config.MapImplementation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class VetMapService extends AbstractMapService<Vet, Long> implements VetService {

    private static final Map<Long, Vet> resource = new HashMap<>();

    @Override
    public Map<Long, Vet> getResource() {
        return resource;
    }

    @Override
    public Collection<Vet> findBySpec(String spec) {
        return null;//todo
    }
}
