package com.example.petclinic.services.jpa;

import com.example.petclinic.entity.Owner;
import com.example.petclinic.entity.Pet;
import com.example.petclinic.services.PetService;
import com.example.petclinic.services.config.JpaImplementation;
import org.springframework.data.jpa.repository.JpaRepository;

@JpaImplementation
public class PetJpaService extends AbstractJpaService<Pet, Long> implements PetService {

    @Override
    public Pet findByOwner(Owner owner) {
        throw new UnsupportedOperationException();
    }

    @Override
    public JpaRepository<Pet, Long> getRepository() {
        throw new UnsupportedOperationException();
    }
}
