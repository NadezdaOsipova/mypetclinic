package com.example.petclinic.cache;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "cache.ttf")
public class CacheProperties {

    private int pets;
    private int vets;
    private int owners;
}
