package com.example.petclinic.repository;

import com.example.petclinic.entity.Pet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PetRepository extends JpaRepository<Pet, Long> {

    List<Pet> findByNameAndOwner_ContactDetailsPhone(String name, String phone);

    @Query("SELECT p FROM Pet p where p.name like : petName")
    Optional<Pet> findByName(@Param("petName") String name);

    @Query(value = "SELECT p FROM Pet p where p.name like : name OR p.owner.firstName12 like :name", nativeQuery = true)
    Optional<Pet> findByNameOrOwner(@Param("name") String name);


}
