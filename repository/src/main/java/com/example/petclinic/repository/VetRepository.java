package com.example.petclinic.repository;

import com.example.petclinic.entity.Vet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VetRepository extends PersonRepository<Vet, Long> {
}
