package com.example.petclinic.repository;

import com.example.petclinic.entity.Owner;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OwnerRepository extends PersonRepository<Owner, Long> {

    List<Owner> findByCreateByAndLastName(String createBy, String lastName);

    List<Owner> findByLastNameOrFirstName(String lastName, String firstName);

    List<Owner> findByLastNameIgnoreCase(String lastName);

    List<Owner> findByLastNameOrFirstNameAllIgnoreCase(String lastName, String firstName);

    List<Owner> findByLastNameContaining(String name);

    List<Owner> findByLastNameLike(String name);

    List<Owner> findByLastNameAndContactDetailsPhone(String lastName, String phone);
}
