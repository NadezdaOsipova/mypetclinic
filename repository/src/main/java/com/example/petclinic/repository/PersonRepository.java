package com.example.petclinic.repository;

import com.example.petclinic.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface PersonRepository<T extends Person, ID> extends JpaRepository<T, ID> {

    List<T> findByFirstName(String firstName);

    List<T> findByLastName(String lastName);

    List<T> findByFullname(String fullname);

}
