package com.example.petclinic.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@SuperBuilder
@MappedSuperclass
public abstract class Person extends BaseEntity<Long> {

    private String firstName;
    private String lastName;
    private String fullname;

    @PrePersist
    @PreUpdate
    private void initFullName() {
        this.fullname = getFirstName() + " " + getLastName();
    }
}
