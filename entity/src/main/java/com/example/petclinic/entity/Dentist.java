package com.example.petclinic.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;

@NoArgsConstructor
@SuperBuilder
@Setter
@Getter
@Entity
public class Dentist extends Vet {

    private String dentistDescription;
}
