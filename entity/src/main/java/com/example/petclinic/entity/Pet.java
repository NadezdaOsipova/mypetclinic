package com.example.petclinic.entity;

import com.example.petclinic.converter.PetTypeConverter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Getter
@Setter
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "pet_type", discriminatorType = DiscriminatorType.STRING)
@Entity
public class Pet extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @ManyToOne
    private Owner owner;
    private LocalDate birthDate;

    @OneToMany(mappedBy = "pet")
    private List<Visit> visits;

    @Column(name = "pet_type", insertable = false, updatable = false)
    @Convert(converter = PetTypeConverter.class)
    private PetType petType;
}
