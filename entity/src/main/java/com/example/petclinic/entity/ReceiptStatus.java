package com.example.petclinic.entity;

public enum ReceiptStatus {

   PENDING, PAYED, CANCELLED
}
