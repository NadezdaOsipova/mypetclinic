package com.example.petclinic.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import com.example.petclinic.annotation.View;
import lombok.Getter;
import lombok.Setter;

//@Setter no updates!
@Getter
@Entity
@View
@Table(name = "owner_receipt")
public class OwnerReceipt {

    @Id
    private Long id;

    private Long receiptId;

    private BigDecimal totalAmount;

    @Column(name = "payer")
    private String payerName;

    @Column(name = "payer_phone")
    private String payerPhone;
}
