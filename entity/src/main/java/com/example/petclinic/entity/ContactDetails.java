package com.example.petclinic.entity;

import lombok.*;

import javax.persistence.Embeddable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@Embeddable
public class ContactDetails {

    private String phone;
    private String city;
    private String street;
    private String building;
}
