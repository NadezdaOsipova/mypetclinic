package com.example.petclinic.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Getter
@Setter
@Entity
@PrimaryKeyJoinColumn(name = "owner_id")
public class VipClient extends Owner {

    private String vipCard;
    private Long discount;
    private String loyaltyProgram;
    private String loyaltyAmount;
}
